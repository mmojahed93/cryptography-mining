import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;

/**
 * Created by noname on 12/14/18.
 */

class BruteforceLimitedHashGenerationRunnable implements Runnable {

    static final String TEXT_PREFIX = "Tarbiat Modares University";
    static final int hashIndex = 15;

    private Thread t;
    private String threadName;
    private int minLength;
    private int maxLength;
    private final HashMap<String, String> hashMap;

    BruteforceLimitedHashGenerationRunnable(String name, int minLength, int maxLength, HashMap<String, String> hashMap) {
        this.threadName = name;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.hashMap = hashMap;

        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);

        final long startTime = System.currentTimeMillis();
        System.out.println("Thread: " + threadName + " Started at: " + startTime +
                " minLength: " + minLength + " maxLength: " + maxLength);

        try {

            System.out.println("Thread: " + threadName + " Time: " + startTime + " Starting hashs generating");

            long startSearchTime = System.currentTimeMillis();
            System.out.println("Thread: " + threadName + " Time: " + startSearchTime + " Starting search, Hash length: " + hashMap.size());

            Bruteforce bruteforce = new Bruteforce(minLength, maxLength) {

                public void element(char[] result, int offset, int length) {

                    String postfix = new String(result, offset, length);
                    String targetText = TEXT_PREFIX + postfix;
                    String hash = DigestUtils.sha1Hex(targetText);

                    String key = hash.substring(0, hashIndex);
                    if (hashMap.containsKey(key)) {
                        System.out.println("Found! " + " Thread: " + threadName + " Old => " + TEXT_PREFIX + hashMap.get(key) + " New => " + " Hash: " + hash + " Text: " + targetText);
                    }
                }
            };
            bruteforce.generate("Z0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY");

        } catch (Exception e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }

        final long endTime = System.currentTimeMillis();
        final long duration = endTime - startTime;
        System.out.println("Thread " + threadName + " exiting. Ended at: " + endTime + " duration: " + duration + " millisecond");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class IdenticalHashSearch {

    public static void main(String[] args) {

        HashMap<String, String> hashMap = initializeHashMap(1, 10, 46500000);

        int threadsNumber = 4;
        int startMinLength = 12;
        for (int i = 0; i < threadsNumber; i++) {
            BruteforceLimitedHashGenerationRunnable thread = new BruteforceLimitedHashGenerationRunnable("Thread-" + (i + 1),
                    startMinLength, startMinLength, hashMap);
            thread.start();

            startMinLength++;
        }

    }

    private static HashMap<String, String> initializeHashMap(int generatingMinLength, int generatingMaxLength, int limit) {
        System.out.println("Starting init hashMap, Time: " + System.currentTimeMillis());
        HashMap<String, String> hashMap = new HashMap<>();

        LimitedBruteforce limitedBruteforce = new LimitedBruteforce(generatingMinLength, generatingMaxLength, limit) {

            public void element(char[] result, int offset, int length) {

                String postfix = new String(result, offset, length);
                String targetText = BruteforceLimitedHashGenerationRunnable.TEXT_PREFIX + postfix;
                String hash = DigestUtils.sha1Hex(targetText);

                String key = hash.substring(0, BruteforceLimitedHashGenerationRunnable.hashIndex);
                if (hashMap.containsKey(key)) {
                    String value = hashMap.get(key);
                    System.out.println("Duplicate! " + " Old => " + value + " New => Hash: " + hash + " Text: " + targetText);
                }
                hashMap.put(key, postfix);
            }
        };
        limitedBruteforce.generate(FindZeroHashWithBruteforce.ALPHABETS);

        return hashMap;
    }
}


