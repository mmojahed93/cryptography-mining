import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by noname on 12/14/18.
 */

class BruteforceRunnable implements Runnable {

    private static final String TEXT_PREFIX = "Tarbiat Modares University";

    private Thread t;
    private String threadName;
    private int minLength;
    private int maxLength;


    BruteforceRunnable(String name, int minLength, int maxLength) {
        threadName = name;
        this.minLength = minLength;
        this.maxLength = maxLength;

        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);
        try {
            final long currentMillSeconds1 = System.currentTimeMillis();
            System.out.println("Thread: " + threadName + " Started at: " + currentMillSeconds1 +
                    " Min length: " + minLength + " Max Length: " + maxLength);

            Bruteforce bruteforce = new Bruteforce(minLength, maxLength) {

                public void element(char[] result, int offset, int length) {
                    String postfix = new String(result, offset, length);
                    String targetText = TEXT_PREFIX + postfix;
                    String hash = DigestUtils.sha1Hex(targetText);

                    if (hash.startsWith("000000000")) {
                        System.out.println("Time: " + System.currentTimeMillis() + " Thread: " + threadName + "**************** Text: " + targetText + " Hash: " + hash);
                    }
                }
            };
            bruteforce.generate(FindZeroHashWithBruteforce.ALPHABETS);

        } catch (Exception e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }

        System.out.println("Thread " + threadName + " exiting.");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class FindZeroHashWithBruteforce {
    public static final String ALPHABETS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static void main(String[] args) {

        int numberOfThread = 4;
        int startMinLength = 4;
        int lengthStep = 2;
        for (int i = 0; i < numberOfThread; i++) {
            BruteforceRunnable thread = new BruteforceRunnable("Thread-" + (i + 1), startMinLength, startMinLength + lengthStep);
            thread.start();
            startMinLength = startMinLength + lengthStep + 1;
        }

    }
}
