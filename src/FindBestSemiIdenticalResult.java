import org.apache.commons.codec.digest.DigestUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by noname on 12/14/18.
 */
public class FindBestSemiIdenticalResult {
    public static void main(String[] args) {

        int identicalIndex = 17;
        String filePath = "/media/noname/Lable1/workspaces/cryptography/src/same-hash.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            for (String line; (line = br.readLine()) != null; ) {
                // process the line.
                if (line.isEmpty() || !line.startsWith("Found!")) {
                    continue;
                }
                String firstText = line.substring(line.indexOf("Old =>") + 7, line.indexOf("New =>") - 1);
                String secondText = line.substring(line.indexOf("Text:") + 6);

                String fhash = DigestUtils.sha1Hex(firstText);
                String shash = DigestUtils.sha1Hex(secondText);
                String fstart = fhash.substring(0, identicalIndex);
                String sstart = shash.substring(0, identicalIndex);
                if (fstart.equals(sstart)) {
                    System.out.println("First Hash: " + fhash + " Second Hash: " + shash + " First Text: " + firstText + " Second Text: " + secondText);
                }
            }
            // line is not visible here.
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
