import org.apache.commons.codec.digest.DigestUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;

/**
 * Created by noname on 12/14/18.
 */

class BruteforceHashGenerationRunnable implements Runnable {

    private static final String TEXT_PREFIX = "Tarbiat Modares University";

    private Thread t;
    private String threadName;
    private int minLength;
    private int maxLength;


    BruteforceHashGenerationRunnable(String name, int minLength, int maxLength) {
        threadName = name;
        this.minLength = minLength;
        this.maxLength = maxLength;

        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);

        final long startTime = System.currentTimeMillis();
        System.out.println("Thread: " + threadName + " Started at: " + startTime +
                " Min length: " + minLength + " Max Length: " + maxLength);

        final HashSet<String> hashSet = new HashSet<>();

        try {

            Bruteforce bruteforce = new Bruteforce(minLength, maxLength) {

                int counter = 0;

                public void element(char[] result, int offset, int length) {

                    String postfix = new String(result, offset, length);
                    String targetText = TEXT_PREFIX + postfix;
                    String hash = DigestUtils.sha1Hex(targetText);
                    hashSet.add("Text: " + targetText + " Hash: " + hash + "\n");
                    counter++;

                    if (counter == 1000) {
                        System.out.println("Thread: " + threadName + " Counter: " + counter + " Start saving to file");

                        try (FileWriter fw = new FileWriter("/media/noname/Lable1/workspaces/cryptography/src/generated_hashs.txt", true);
                             BufferedWriter bw = new BufferedWriter(fw);
                             PrintWriter out = new PrintWriter(bw)) {

                            out.println(hashSet + "");
                        } catch (IOException e) {
                            //exception handling left as an exercise for the reader
                            e.printStackTrace();
                        }

                        counter = 0;
                        hashSet.clear();

                        System.out.println("Thread: " + threadName + " Counter: " + counter + " Saved to file successfully");
                    }

                }
            };
            bruteforce.generate(FindZeroHashWithBruteforce.ALPHABETS);

        } catch (Exception e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }

        final long endTime = System.currentTimeMillis();
        final long duration = endTime - startTime;
        System.out.println("Thread " + threadName + " exiting. Ended at: " + endTime + " duration: " + duration + " millisecond");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class GenerateHashes {

    public static void main(String[] args) {
        BruteforceHashGenerationRunnable thread = new BruteforceHashGenerationRunnable("Thread-1", 9, 9);
        thread.start();
    }
}

