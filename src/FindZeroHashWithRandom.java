import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by noname on 12/14/18.
 */
class RunnableSample implements Runnable {

    public static final String TEXT_PREFIX = "Tarbiat Modares University";

    private Thread t;
    private String threadName;
    private int lengthOfRandom;

    RunnableSample(String name, int lengthOfRandom) {
        threadName = name;
        this.lengthOfRandom = lengthOfRandom;
        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName + " With random length: " + lengthOfRandom);

        try {

            final String seed = System.currentTimeMillis() + "";
            final RandomString session = new RandomString(lengthOfRandom, seed);
            final int maxRandomLength = 20;
            final int loop = 100000000;

            System.out.println("Thread: " + threadName + " Seed: " + seed);

            while (lengthOfRandom < maxRandomLength) {
                final long currentMillSeconds1 = System.currentTimeMillis();
                System.out.println("Thread: " + threadName + " Random Length: " + lengthOfRandom + " Start at: " + currentMillSeconds1);

                for (int i = 0; i < loop; i++) {
                    String randomString = TEXT_PREFIX + session.nextString();
                    String hash = DigestUtils.sha1Hex(randomString);
                    if (hash.startsWith("0000000")) {
                        System.out.println("**************** Thread: " + threadName + " Text: " + randomString + " Hash: " + hash);
                    }
                }

                final long currentMillSeconds2 = System.currentTimeMillis();
                final long duration = (currentMillSeconds2 - currentMillSeconds1) / 1000;
                System.out.println("Thread: " + threadName + " Total loop: " + loop + " duration: " + duration + "'s");

                this.lengthOfRandom++;
            }


        } catch (Exception e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }

        System.out.println("Thread " + threadName + " exiting.");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}


public class FindZeroHashWithRandom {

    public static void main(String args[]) {

        int numberOfThread = 4;
        for (int i = 0; i < numberOfThread; i++) {
            RunnableSample thread = new RunnableSample("Thread-" + (i + 1), 4);
            thread.start();
        }

    }
}